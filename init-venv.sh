#!/bin/bash -e

PROJECT_USER="django"

if [ $USER != $PROJECT_USER ]
then
    echo "You must login as $PROJECT_USER to activate env (sudo -i -u $PROJECT_USER)"
    return
fi

PROJECT_DIR=$(pwd)
PROJECT_REQUIREMENTS="$PROJECT_DIR/requirements.txt"

VENV_NAME="venv"
VENV_PATH="$PROJECT_DIR/$VENV_NAME"
VENV_PYTHON_VERSION="/usr/bin/python3"

export DJANGO_SETTINGS_MODULE=generate_adverts.settings_production

if [ ! -d "$VENV_PATH" ]
then
	virtualenv "$VENV_PATH" -p "$VENV_PYTHON_VERSION" --no-site-packages
	$echo "Project virtualenv created"
fi

source $VENV_PATH/bin/activate
echo "Project virtualenv activated"

if [ ! -f "$VENV_PATH/updated" -o "$PROJECT_REQUIREMENTS" -nt "$VENV_PATH/updated" ]; then
    pip install -r "$PROJECT_REQUIREMENTS"
    touch "$VENV_PATH/updated"
    echo "Requirements installed."
fi




