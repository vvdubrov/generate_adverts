(function($) {
    
    /*
    *   Загружает картинку file_content через FileReader,
    *   помещает её в container, внутри которого позиционирует 
    *   картинку по центру.
    */
    window.centerReadedFile = function(file_content, container, callback) {
        if (window.FileReader) {
            var reader = new FileReader();
            reader.onload = function (event) {
                var preview = $('<img>').attr('src', event.target.result),
                    preview_width = container.width(),
                    preview_height = container.height();
                
                container.children('img').remove();
                container.css({
                    background: 'none'
                }).prepend(
                    preview
                );
                
                // Callback
                if (callback) {
                    callback(container);
                }
                
                // Ставим картинку по центру
                preview.width(preview_width);
                if (preview.height() < preview_height) {
                    preview.width('').height(preview_height)
                }
                preview.css({
                    left: Math.round((preview_width - preview.width()) / 2),
                    top: Math.round((preview_height - preview.height()) / 2)
                })
            };
            reader.readAsDataURL(file_content);
        }
    }
    
})((window.django && django.jQuery) || jQuery);