from .settings import *

SECRET_KEY = 'kul%w9j2&o*4ke_=#_#9z8-yse93u7q8y&+p5p85e7wrhav*pr'

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
