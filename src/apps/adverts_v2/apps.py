from django.apps import AppConfig


class AdvertsV2Config(AppConfig):
    name = 'apps.adverts_v2'
