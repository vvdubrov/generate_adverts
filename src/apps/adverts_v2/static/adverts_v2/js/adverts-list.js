(function ($) {

    var makeAlert = function (style, message) {
        return '<div class="alert alert-' + style + ' alert-dismissible" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span></button>' + message + '</div>'
    };

    var bindGalleries = function () {
        $('.advert-item').each(function () {
            $(this).find('.advert-photo-wrapper a').magnificPopup({
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        });
    };

    $(document).on('click', '#load-more-button', function () {
        var $button = $(this);
        $.ajax({
            url: $button.attr('href'),
            type: 'GET',
            cache: false,
            beforeSend: function () {
                $button.button('loading');
            },
            success: function (response) {
                $button.closest('.row').replaceWith(response.rendered_adverts_list);
                bindGalleries();
            }
        });
        return false;
    });

    $(document).on('change', '.advert-photo-controls input[type="checkbox"]', function () {
        var $checkBox = $(this),
            $imageWrapper = $checkBox.closest('.advert-photo-wrapper'),
            value = $checkBox.prop('checked');

        $.ajax({
            url: '/v2/change-photo-status/' + $checkBox.closest('.advert-photo-controls').data('photo-id') + '/',
            type: 'GET',
            data: {status: $checkBox.prop('checked')},
            beforeSend: function () {
                $imageWrapper.addClass('loader');
            },
            success: function (response) {
                if (response.error) {
                    alert(response.error);
                    $checkBox.prop('checked', !value);
                }
            },
            complete: function () {
                $imageWrapper.removeClass('loader');
            }
        });
        return false;
    });

    $(document).on('click', '.advert-renew-desc', function () {
        var $button = $(this),
            $descriptionWrapper = $button.closest('.advert-item').find('.advert-description');

        $.ajax({
            url: '/v2/refresh-description/' + $button.closest('.advert-controls').data('advert-id') + '/',
            type: 'GET',
            beforeSend: function () {
                $button.prop('disabled', true);
                $descriptionWrapper.addClass('loader');
            },
            success: function (response) {
                if (response.error) {
                    alert(response.error);
                } else {
                    $descriptionWrapper.text(response.text)
                }
            },
            complete: function () {
                $button.prop('disabled', false);
                $descriptionWrapper.removeClass('loader');
            }
        });
        return false;
    });

    $(document).on('click', '.advert-add-to-work', function () {
        $(this).closest('.advert-item').find('.advert-status-change[data-status-id=3]').trigger('click');
    });
    
    $(document).on('click', '.advert-reject', function () {
        $(this).closest('.advert-item').find('.advert-status-change[data-status-id=1]').trigger('click');
    });

    $(document).on('click', '#work-complete', function (event) {
        if (confirm('Вы уверены, что хотите заверить работу с объявлениями. ' +
                'Все объявления пометятся как использованные.')) {
            $(event.target).button('loading');
            $.getJSON($(event.target).attr('href'), function (response) {
                window.location.href = response.redirect
            });
        }
        event.preventDefault();
    });

    $(document).on('click', 'li:not(.disabled) > .advert-status-change', function () {
        var $button = $(this),
            $advertItem = $button.closest('.advert-item'),
            $loaderWrapper = $advertItem.children().first();

        $.ajax({
            url: '/v2/change-advert-status/' + $button.closest('.advert-controls').data('advert-id') + '/',
            type: 'GET',
            data: {status: $button.data('status-id')},
            beforeSend: function () {
                $button.parent().addClass('disabled');
                $loaderWrapper.addClass('loader');
            },
            success: function (response) {
                if (response.error) {
                    $loaderWrapper.removeClass('loader');
                    alert(response.error);
                    return;
                }
                $advertItem.fadeOut("normal", function () {
                    $advertItem.remove();
                    $.each(response.items, function (key, value) {
                        $('#menu-status-' + key).find('.badge').text(value);
                    });
                    if (!$('.advert-item').length) {
                        $('#load-more-button').trigger('click');
                    }
                });
            },
            error: function () {
                $loaderWrapper.removeClass('loader');
            },
            complete: function () {
                $button.parent().removeClass('disabled');
            }
        });
        return false;
    });

    $(document).on('click', '.advert-edit', function () {
        var $button = $(this),
            $modal = $('#advert-form-modal'),
            advertID = $button.closest('.advert-controls').data('advert-id'),
            url = '/v2/change-advert/' + advertID + '/';

        $.ajax(url, {
            beforeSend: function () {
                $button.button('loading');
            },
            success: function (response) {
                if (response.error) {
                    alert(response.error);
                    return;
                }
                $modal.find('.modal-title').text('Редактирование объявления #' + advertID);
                $modal.find('.modal-body').html(response);
                $modal.modal();
                $modal.one('shown.bs.modal', function () {
                    $modal.find('.advert-save, .advert-save-and-close').one('click', function () {
                        var $submitButton = $(this),
                            $form = $modal.find('form');
                        $.ajax(url, {
                            method: $form.prop('method') || 'post',
                            data: $form.serialize(),
                            beforeSend: function () {
                                $submitButton.button('loading');
                                $form.find('.has-error').removeClass('has-error');
                                $modal.find('.error-text, .message-box').empty();
                            },
                            success: function (submitResponse) {
                                if (submitResponse.error) {
                                    alert(submitResponse.error);
                                    return;
                                }

                                if (submitResponse.form_errors) {
                                    $modal.find('.message-box').html(makeAlert('danger', 'Не удалось сохранить объявление. ' +
                                        'Исправьте ошибки и попробуйте снова.'));

                                    $.each(submitResponse.form_errors, function (fieldName, errors) {
                                        if (typeof fieldName != 'string') {
                                            return;
                                        }

                                        if (fieldName == '__all__') {
                                            alert(errors[fieldName].join('\n'));
                                            return;
                                        }

                                        $form.find('#id_' + fieldName).closest('.form-group').addClass('has-error')
                                            .find('.error-text').text(errors.join(', '));
                                    });
                                    return;
                                }

                                if (submitResponse.updated_advert) {
                                    $('#advert-' + advertID).replaceWith(submitResponse.updated_advert);
                                    bindGalleries();
                                }

                                if ($submitButton.hasClass('advert-save-and-close')) {
                                    $modal.modal('hide');
                                    return;
                                }

                                var successMessage;
                                if (submitResponse.not_changed) {
                                    successMessage = makeAlert('info', 'Данные объявления не изменились')
                                } else {
                                    successMessage = makeAlert('success', 'Данные объявления обновлены')
                                }
                                $modal.find('.message-box').html(successMessage);
                            },
                            complete: function () {
                                $submitButton.button('reset');
                            }
                        });
                    })
                });
                $modal.on('hidden.bs.modal', function () {
                    $modal.find('.modal-body, .modal-title').empty();
                });
            },
            complete: function () {
                $button.button('reset');
            }
        });
        return false;
    });

    $(document).on('click', '#bulk-create', function () {
        var $button = $(this),
            $modal = $('#adverts-bulk-create'),
            url = $button.attr('href');

        $.ajax(url, {
            beforeSend: function () {
                $button.button('loading');
            },
            success: function (response) {
                if (response.error) {
                    alert(response.error);
                    return;
                }
                $modal.find('.modal-body').html(response);
                $modal.modal();
                $modal.one('shown.bs.modal', function () {
                    $modal.find('.adverts-create').one('click', function () {
                        var $submitButton = $(this),
                            $form = $modal.find('form');
                        $.ajax(url, {
                            method: $form.prop('method') || 'post',
                            data: $form.serialize(),
                            beforeSend: function () {
                                $submitButton.button('loading');
                                $form.find('.has-error').removeClass('has-error');
                                $modal.find('.error-text, .message-box').empty();
                            },
                            success: function (submitResponse) {
                                if (submitResponse.error) {
                                    alert(submitResponse.error);
                                    return;
                                }

                                if (submitResponse.form_errors) {
                                    $modal.find('.message-box').html(makeAlert('danger', 'Не удалось создать объявления. ' +
                                        'Исправьте ошибки и попробуйте снова.'));

                                    $.each(submitResponse.form_errors, function (fieldName, errors) {
                                        if (typeof fieldName != 'string') {
                                            return;
                                        }

                                        if (fieldName == '__all__') {
                                            alert(errors[fieldName].join('\n'));
                                            return;
                                        }

                                        $form.find('#id_' + fieldName).closest('.form-group').addClass('has-error')
                                            .find('.error-text').text(errors.join(', '));
                                    });
                                    return;
                                }

                                $.get(submitResponse.link, function (data) {
                                    $('#adverts-list').html(data.rendered_adverts_list);
                                    $modal.find('.message-box').html(makeAlert('success', 'Создание объявлений завершено'));
                                    bindGalleries();
                                    $submitButton.button('reset');
                                });

                            },
                            error: function () {
                                $submitButton.button('reset');
                            }
                        });
                    });
                    $modal.on('hidden.bs.modal', function () {
                        $modal.find('.modal-body').empty();
                    });
                });
            },
            complete: function () {
                $button.button('reset');
            }
        });
        return false;
    });

    $(document).on('click', '.set-original', function () {
        var $button = $(this),
            $advertWrapper = $button.closest('.advert-item');

        $.ajax({
            url: '/v2/set-original-description/' + $advertWrapper.find('.advert-controls').data('advert-id') + '/',
            type: 'GET',
            data: {field: $button.data('field')},
            beforeSend: function () {
                $button.prop('disabled', true);
            },
            success: function (response) {
                if (response.error) {
                    alert(response.error);
                    $button.prop('disabled', false);
                } else {
                    $advertWrapper.replaceWith(response.updated_advert);
                    bindGalleries();
                }
            }
        });
        return false;
    });

    $(document).ready(bindGalleries);

})(jQuery);