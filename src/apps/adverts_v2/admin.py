from django import forms
from django.contrib import admin
from . models import *


class CategoryParsingSettingsInline(admin.StackedInline):
    model = CategoryParsingSettings
    extra = 0


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', )
    search_fields = ('title', )
    prepopulated_fields = {'alias': ('title', )}
    inlines = (CategoryParsingSettingsInline, )

    fieldsets = (
        (None, {
            'fields': ('title', 'alias')
        }),
        ('Настройки', {
            'fields': ('model_name',  'builder_class_name')
        }),
    )


class AdvertPhotoInline(admin.TabularInline):
    model = AdvertPhoto
    extra = 0


class AdvertInWorkInline(admin.TabularInline):
    model = AdvertInWork
    extra = 0


class BaseAdvertAdmin(admin.ModelAdmin):
    list_display = ('category', 'donor', 'visible')
    list_filter = ('category', 'donor', 'visible')
    inlines = (AdvertInWorkInline, AdvertPhotoInline, )
    readonly_fields = ('donor', 'donor_url', 'created', 'updated')

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields['title'].widget = forms.TextInput(attrs={'style': 'width:610px'})
        return form


@admin.register(Advert)
class CommonAdvertAdmin(BaseAdvertAdmin):
    fieldsets = (
        (None, {
            'fields': ('status', 'title_original', 'title', 'description_original', 'description', 'price', 'visible')
        }),
        ('Адрес', {
            'fields': ('city',)
        }),
        ('Техническая информация', {
            'fields': ('donor', 'donor_url', 'created', 'updated')
        }),
    )


@admin.register(ApartmentAdvert)
class ApartmentAdvertAdmin(BaseAdvertAdmin):
    fieldsets = (
        (None, {
            'fields': ('status', 'title_original', 'title', 'description_original', 'description', 'price', 'visible')
        }),
        ('Адрес', {
            'fields': ('city', 'district', 'street', 'house_number')
        }),
        ('Основная детализация', {
            'fields': ('rooms_number', 'floor', 'floors_total', 'area_living', 'area_total', 'area_kitchen',
                       'ceiling_height', 'beds_number', 'condition', 'apartment_type')
        }),
        ('Удобства', {
            'fields': ('has_furniture', 'has_kitchen', 'has_refrigerator', 'has_washing_machine',
                       'has_conditioner', 'has_tv', 'has_internet')
        }),
        ('Техническая информация', {
            'fields': ('donor', 'donor_url', 'created', 'updated')
        }),
    )

