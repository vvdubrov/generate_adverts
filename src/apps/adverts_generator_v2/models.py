import logging

from django.db import models

from .exceptions import SkipPhrase
from .utils import process_text, process_text_with_object
from libs.utils import get_random_bool
from apps.adverts_v2.options import AUTO_GENERATED_FIELDS

__all__ = ('PhraseGroupSet', 'PhraseGroup', 'GroupInPhraseGroupSet', 'Phrase')


class PhraseGroupSet(models.Model):
    category = models.ForeignKey('adverts_v2.Category', verbose_name='Категория')
    advert_field = models.CharField(verbose_name='Поле', max_length=50, choices=[(c, c) for c in AUTO_GENERATED_FIELDS])
    title = models.CharField(verbose_name='Название', max_length=100, unique=True)

    class Meta:
        verbose_name = 'набор групп фраз'
        verbose_name_plural = 'наборы групп фраз'

    def __str__(self):
        return self.title

    def get_random_phrase_templates(self):
        for group in self.groups.not_empty():
            if group.required or get_random_bool():
                yield from group.phrase_group.phrases.order_by('?')[:group.phrases_limit]

    def get_processed_phrases(self, obj):
        for phrase in self.get_random_phrase_templates():
            try:
                yield phrase.make_phrase_from_object(obj)
            except SkipPhrase as error:
                logging.warning('{0} skipped: {1}'.format(phrase, error))

    def generate_text_from_object(self, obj):
        return ' '.join(phrase for phrase in self.get_processed_phrases(obj))


class PhraseGroup(models.Model):
    title = models.CharField(verbose_name='Название', max_length=100, unique=True)

    class Meta:
        verbose_name = 'группа фраз'
        verbose_name_plural = 'группы фраз'

    def __str__(self):
        return self.title


class GroupInPhraseGroupSetQueryset(models.QuerySet):
    def not_empty(self):
        return self.exclude(phrase_group__phrases=None)


class GroupInPhraseGroupSet(models.Model):
    phrase_groupset = models.ForeignKey(PhraseGroupSet, verbose_name='Объект описания', related_name='groups')
    phrase_group = models.ForeignKey(PhraseGroup, verbose_name='Группа фраз')
    required = models.BooleanField(verbose_name='Не пропускать', default=True)
    phrases_limit = models.PositiveSmallIntegerField(verbose_name='Количество фраз', default=1)
    order = models.PositiveIntegerField(verbose_name='Порядок', default=0)

    objects = GroupInPhraseGroupSetQueryset.as_manager()

    class Meta:
        verbose_name = 'группа фраз в наборе'
        verbose_name_plural = 'группы фраз в наборе'
        unique_together = (('phrase_groupset', 'phrase_group'), )
        ordering = ('order', )


class Phrase(models.Model):
    group = models.ForeignKey(PhraseGroup, verbose_name='Группа', related_name='phrases')
    text = models.TextField(verbose_name='Фраза')

    class Meta:
        verbose_name = 'фраза'
        verbose_name_plural = 'фразы'

    def __str__(self):
        return self.text

    def make_phrase(self):
        return process_text(self.text)

    def make_phrase_from_object(self, obj):
        return process_text_with_object(self.text, obj)
