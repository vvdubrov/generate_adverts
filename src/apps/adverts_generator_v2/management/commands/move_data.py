from django.core.management import BaseCommand

from apps.adverts_generator.models import PhraseGroupSet as OldPhraseGroupSet
from apps.adverts_generator_v2.models import PhraseGroupSet, PhraseGroup, Phrase
from apps.adverts_v2.models import Category


class Command(BaseCommand):

    def handle(self, *args, **options):
        for old_phrase_groupset in OldPhraseGroupSet.objects.all():
            category = Category.objects.get(title__iexact=old_phrase_groupset.get_category_display())
            new_phrase_groupset = PhraseGroupSet.objects.create(
                category=category,  advert_field=old_phrase_groupset.advert_field,
                title=old_phrase_groupset.title)

            for old_phrase_group in old_phrase_groupset.groups.all():
                new_phrase_group = PhraseGroup.objects.create(title=old_phrase_group.phrase_group.title)
                new_phrase_groupset.groups.create(
                    phrase_group=new_phrase_group,
                    required=old_phrase_group.required,
                    phrases_limit=old_phrase_group.phrases_limit,
                    order=old_phrase_group.order
                )
                for old_phrase in old_phrase_group.phrase_group.phrases.all():
                    new_phrase_group.phrases.create(text=old_phrase.text)