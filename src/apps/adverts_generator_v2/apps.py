from django.apps import AppConfig


class AdvertsGeneratorV2Config(AppConfig):
    name = 'apps.adverts_generator_v2'
