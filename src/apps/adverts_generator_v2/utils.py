import re
import random

import collections

from django.apps import apps

from .exceptions import PhraseTemplateError, SkipPhrase
from libs.utils import ChoicesHelper

__all__ = ('process_text', 'process_text_with_object')

OBJECT_TAG_REGEX = re.compile(r'{%\s*\b([a-zA-Z_.]+)\b\s*%}')
CHOICE_TAG_REGEX = re.compile(r'{{\s*([^\{\}]*)\s*}}')
CHOICE_TAG_DELIMITER = '|'


def _process_choice_tags(text):
    for match in CHOICE_TAG_REGEX.finditer(text):
        text = text.replace(match.group(), random.choice(match.groups()[0].split(CHOICE_TAG_DELIMITER)))
    return text


def _chain_attribute_getter(obj, attributes, default=None):
    try:
        for attribute in attributes:
            attr = obj[attribute] if isinstance(object, collections.Mapping) else getattr(obj, attribute)
            obj = attr() if callable(attr) else attr
    except (KeyError, AttributeError) as error:
        raise PhraseTemplateError('Failed to get attribute: {}'.format(error))
    else:
        return obj


def _process_object_tags(text, obj):
    for match in OBJECT_TAG_REGEX.finditer(text):
        attributes = match.groups()[0].split('.')
        try:
            value = _chain_attribute_getter(obj, attributes)
        except PhraseTemplateError as error:
            raise SkipPhrase(str(error))
        else:
            if isinstance(value, bool):
                value = 'да' if value else 'нет'
            elif not value:
                raise SkipPhrase('Empty value received')
            text = text.replace(match.group(), str(value))
    return text.strip()


def process_text(text):
    text = _process_choice_tags(text)
    return text.strip()


def process_text_with_object(text, obj):
    text = _process_object_tags(text, obj)
    return process_text(text)
