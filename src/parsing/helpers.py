import re
from urllib.parse import urlparse

__all__ = ('normalize_int', 'normalize_decimal')


REGEX_INT = re.compile(r'[^\d]+')
REGEX_DECIMAL = re.compile(r'[^\d.,]+')


def normalize_choice_by_word(choices_iterable, word, aliases_key='aliases'):
    try:
        word = word.lower()
    except AttributeError:
        pass

    for value, title, extra in choices_iterable:
        if title.lower() == word or word in extra.get(aliases_key, []):
            return value


def normalize_int(val, default=None):
    """
    Пытается привести строку к целому числу
    """
    try:
        res = int(REGEX_INT.sub('', val))
    except ValueError:
        return default
    return res


def normalize_decimal(val, default=None):
    """
    Пытается привести строку к числу с точкой
    """
    try:
        res = float(REGEX_DECIMAL.sub('', val))
    except ValueError:
        return default
    return res


def normalize_string(val):
    return ' '.join(val.split())


def normalize_yes_no(val):
    if normalize_string(val) in ('есть', ):
        return True
    return False


def make_absolute_link(response_url, path):
    return '{parsed.scheme}://{parsed.netloc}{path}'.format(parsed=urlparse(response_url), path=path)
