from PIL import ImageEnhance
from scrapy import Request

from apps.adverts_v2.options import AVITO
from ...helpers import make_absolute_link, normalize_int, normalize_string
from ...base_spider import BaseSpider

__all__ = ('AvitoSpider', )

COLLECT_LINKS_SELECTOR = '//a[@class="item-link"]/@href'
NEXT_PAGE_SELECTOR = '//li[@class="page page-next"]/a/@href'
TITLE_SELECTOR = '//header[@class="single-item-header b-with-padding"]/text()'
DESCRIPTION_SELECTOR = '//div[@class="description-preview-wrapper"]/p/text()'
PRICE_SELECTOR = '//span[@class="price-value"]/text()'
IMAGES_SELECTOR = '//meta[@property="og:image"]/@content'


class AvitoSpider(BaseSpider):
    name = 'avito-spider'
    allowed_domains = ('avito.ru', 'm.avito.ru')
    donor = AVITO

    custom_settings = {
        'CONCURRENT_REQUESTS': 3,
        'DOWNLOAD_DELAY': 5,
        'ITEM_PIPELINES': {
            'parsing.common_pipelines.MakeInitialChecks': 10,
            'parsing.common_pipelines.BuildCommonData': 20,
            'parsing.common_pipelines.ValidateAndSaveItem': 100,
            'parsing.common_pipelines.ProcessItemPhotos': 150,
        },
        'DOWNLOADER_MIDDLEWARES': {
            'parsing.common_middlewares.RandomUserAgentMiddleware': 50,
        },
        'IMAGES_CROP': {
            'bottom': 40,
        },
        'IMAGES_ENHANCE': {
            'enhancer': ImageEnhance.Color,
            'factor': 1.2
        },
    }

    def parse_list(self, response):
        adverts_links = response.xpath(COLLECT_LINKS_SELECTOR).extract()
        if not adverts_links:
            self.logger.warning('No adverts links founded on page {}'.format(response.url))

        for link in adverts_links:
            yield Request(url=make_absolute_link(response.url, link), callback=self.parse_item)

        next_page_link = response.xpath(NEXT_PAGE_SELECTOR).extract_first()
        if next_page_link:
            yield Request(make_absolute_link(response.url, next_page_link), callback=self.parse, dont_filter=True)

    def parse_item(self, response):
        item = super().parse_item(response)
        item['price'] = normalize_int(val=response.xpath(PRICE_SELECTOR).extract_first())
        item['title_original'] = normalize_string(response.xpath(TITLE_SELECTOR).extract_first())
        item['description_original'] = '\n'.join(response.xpath(DESCRIPTION_SELECTOR).extract())
        item['image_urls'] = response.xpath(IMAGES_SELECTOR).extract()
        return item
