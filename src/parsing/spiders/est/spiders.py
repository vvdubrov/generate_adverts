from functools import partial

from scrapy import Selector, Request

from apps.adverts_v2.options import CONDITIONS_FULL, APARTMENT_TYPES_FULL, EST
from parsing.items import ApartmentAdvertItem
from ... import helpers
from ...base_spider import BaseSpider

__all__ = ('EstSpider', )


COLLECT_LINKS_SELECTOR = '//div[@class="eo-item__headline"]/a/@href'
COLLECT_DETALIZATION_SELECTOR = '//table[@class="info-table"]/tr'
IMAGES_SELECTOR = '//div[@class="app__fotorama fotorama"]/a/@href'
NEXT_PAGE_SELECTOR = '//a[@class="paging__next link-black"]/@href'
PRICE_SELECTOR = '//span[@data-currency="RUB"]/@data-price-total'
DESCRIPTION_SELECTOR = '//div[@class="est-easy-html app__promo"]/p/text()'

IMAGES_DOWNLOAD_LIMIT = 20
PAGES_LIMIT = 5


def get_info_mapping():
    return [
        ('количество комнат', 'rooms_number', helpers.normalize_int),
        ('этаж', 'floor', helpers.normalize_int),
        ('этажность', 'floors_total', helpers.normalize_int),
        ('жилая площадь', 'area_living', helpers.normalize_int),
        ('общая площадь', 'area_total', helpers.normalize_int),
        ('площадь кухни', 'area_kitchen', helpers.normalize_int),
        ('общее состояние', 'condition', partial(helpers.normalize_choice_by_word, CONDITIONS_FULL)),
        ('тип квартиры', 'apartment_type', partial(helpers.normalize_choice_by_word, APARTMENT_TYPES_FULL)),
        ('мебель', 'has_furniture', helpers.normalize_yes_no),
        ('кухня', 'has_kitchen', helpers.normalize_yes_no),
        ('холодильник', 'has_refrigerator', helpers.normalize_yes_no),
        ('стиральная машина', 'has_washing_machine', helpers.normalize_yes_no),
        ('кондиционер', 'has_conditioner', helpers.normalize_yes_no),
        ('телевидение', 'has_tv', helpers.normalize_yes_no),
        ('интернет', 'has_internet', helpers.normalize_yes_no),
        ('высота потолков', 'ceiling_height', helpers.normalize_decimal),
        ('количество спальных мест', 'beds_number', helpers.normalize_int),
    ]


class EstSpider(BaseSpider):
    name = 'est-spider'
    item_class = ApartmentAdvertItem
    allowed_domains = ('est.ua', 'estru.ru', 'estcrimea.com')
    donor = EST

    custom_settings = {
        'ITEM_PIPELINES': {
            'parsing.common_pipelines.MakeInitialChecks': 10,
            'parsing.common_pipelines.BuildCommonData': 20,
            'parsing.spiders.est.pipelines.PrepareItemData': 50,
            'parsing.common_pipelines.ValidateAndSaveItem': 100,
            'parsing.common_pipelines.ProcessItemPhotos': 150,
        }
    }

    def parse_list(self, response):
        adverts_links = response.xpath(COLLECT_LINKS_SELECTOR).extract()
        if not adverts_links:
            self.logger.warning('No adverts links founded on page {}'.format(response.url))

        for link in adverts_links:
            yield Request(link, callback=self.parse_item)

        next_page_link = response.xpath(NEXT_PAGE_SELECTOR).extract_first()
        if next_page_link:
            yield Request(next_page_link, callback=self.parse, dont_filter=True)

    def parse_item(self, response):
        item = super().parse_item(response)

        detalization_rows = response.xpath(COLLECT_DETALIZATION_SELECTOR).extract()
        if not detalization_rows:
            self.logger.error('No detalization info found on page {}'.format(response.url))
            return

        info_mapping = get_info_mapping()
        for row in response.xpath(COLLECT_DETALIZATION_SELECTOR).extract():
            sel = Selector(text=row)
            token = sel.xpath('//th/text()').extract_first()
            value = sel.xpath('//td/text()').extract_first()

            if token is None or value is None:
                self.logger.error('Invalid detalization table structure on page {}'.format(response.url))
                continue

            token = token.strip().lower()
            value = value.strip().lower()

            for mapping_item in info_mapping:
                mapping_token, field_name, normalizer = mapping_item
                if token == mapping_token:
                    info_mapping.remove(mapping_item)
                    item[field_name] = normalizer(value)

        item['description_original'] = '\n'.join(response.xpath(DESCRIPTION_SELECTOR).extract())
        item['price'] = helpers.normalize_int(response.xpath(PRICE_SELECTOR).extract_first())
        item['image_urls'] = response.xpath(IMAGES_SELECTOR).extract()[:IMAGES_DOWNLOAD_LIMIT]
        return item
