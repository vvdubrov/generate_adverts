from scrapy_djangoitem import DjangoItem, Field

from apps.adverts_v2.models import Advert, ApartmentAdvert


class AdvertItem(DjangoItem):
    django_model = Advert

    images = Field()
    image_urls = Field()


class ApartmentAdvertItem(AdvertItem):
    django_model = ApartmentAdvert

