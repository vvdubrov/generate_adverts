from django import template


register = template.Library()


@register.filter
def variation(value, arg):
    """
    Получение вариации по имени.
    Если не найдена - возвращается оригинал.
    Пример:
        {% with variation=image|variation:'normal' %}
            {{ variation.url }}
        {% endwith %}
    """
    return getattr(value, arg, value) if arg else value
