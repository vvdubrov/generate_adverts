(function($) {

    var input_image = function(file_input) {
        var files = file_input.prop('files');
        if (!files) {
            return;
        }
        for(var i=0, l=files.length; i<l; i++) {
            var current_file = files[i];
            if (/^image\//.test(current_file.type)) {
                return current_file
            }
        }
    };

    $(document).on('change', '.stdimage .uploader', function(event) {
        var input = $(this),
            block = input.closest('.stdimage'),
            preview = block.find('.item-preview'),
            crop_btn_wrapper = block.find('.crop-btn-wrapper'),
            current_file = input_image(input);

        // Сброс картинки
        if (!current_file) {
            if (!event.isTrigger) {
                crop_btn_wrapper.addClass('hide');
                preview.addClass('hide');
                preview.find('img').css({
                    width: '',
                    height: '',
                    top: 0,
                    left: 0
                })
            }
            return
        }
            
        // Читаем картинку и создаем превью
        centerReadedFile(current_file, preview, function(container) {
            preview.removeClass('hide');
            crop_btn_wrapper.removeClass('hide');
        });
    });


    // Обновление положения превью
    var preview_refresh_crop = function(preview, coords) {
        var preview_img = preview.find('img'),
            file_aspect = coords[2] / coords[3],
            preview_area_size = [parseInt(preview.width()), parseInt(preview.height())],
            preview_aspect = preview_area_size[0] / preview_area_size[1];
        
        // Определяем по какому параметру сжимать
        if ((preview_area_size[0] / file_aspect) > preview_area_size[1]) {
            //Сжатие по ширине
            var new_h = Math.min(coords[3], Math.round(coords[2] / preview_aspect));
            var new_y = coords[1] + (coords[3] - new_h) / 2;
            var rx = preview_area_size[0] / coords[2];
            var ry = preview_area_size[1] / new_h;
            preview_img.css({
                width: Math.round(rx * preview_img.prop('naturalWidth')),
                height: Math.round(ry * preview_img.prop('naturalHeight')),
                left: -Math.round(rx * coords[0]),
                top: -Math.round(ry * new_y)
            })
        } else {
            //Сжатие по высоте
            var new_w = Math.min(coords[2], Math.round(coords[3] * preview_aspect));
            var new_x = coords[0] + (coords[2] - new_w) / 2;
            var rx = preview_area_size[0] / new_w;
            var ry = preview_area_size[1] / coords[3];
            preview_img.css({
                width: Math.round(rx * preview_img.prop('naturalWidth')),
                height: Math.round(ry * preview_img.prop('naturalHeight')),
                left: -Math.round(rx * new_x),
                top: -Math.round(ry * coords[1])
            })
        }
    };

    
    $(document).cropdialog({
        event: 'click.cropdialog',
        event_selector: '.stdimage .crop-btn-wrapper button',
        source_image: function(button) {
            var block = button.closest('.stdimage'),
                image = input_image(block.find('.uploader'));
            if (image) {
                this.loaded = false;
                return block.find('.item-preview img').prop('src');
            }
            
            this.loaded = true;
            return button.data('source') + '?_=' + Math.random().toString().substr(2);
        },
        min_size: function(button) {
            var min_size = button.data('min_dimensions');
            return String(min_size).split('x').map(function(e) {
                return parseInt(e)
            });
        },
        max_size: function(button) {
            var max_size = button.data('max_dimensions');
            return String(max_size).split('x').map(function(e) {
                return parseInt(e)
            });
        },
        aspect: function(button) {
            var aspects = button.data('aspects');
            aspects = String(aspects).split('|').map(function(e) {
                return parseFloat(e)
            });
            return aspects[0];
        },
        crop_position: function(button) {
            var crop_position = button.data('crop');
            return String(crop_position).split(':').map(function(e) {
                return parseInt(e)
            });
        },
        dialog_ok: function(button, dialog, coords) {
            var preview = button.closest('.stdimage').find('.item-preview');
            
            if (this.loaded) {
                var img = new Image;
                img.onload = function() {
                    preview.find('img').attr('src', img.src);
                    preview_refresh_crop(preview, coords);
                };
                img.src = button.data('source') + '?_=' + Math.random().toString().substr(2);
            } else {
                preview_refresh_crop(preview, coords);
            }
            
            // Записываем координаты в форму
            var coords_text = coords.join(':');
            button.next('input').val(coords_text);
            button.data('crop', coords_text);
        }
    });

})(jQuery || django.jQuery);