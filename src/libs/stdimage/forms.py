from django.conf import settings
from django.forms import ImageField
from .widgets import StdImageWidget
from .utils import get_variation_by_name


class StdImageFormField(ImageField):
    preview_size = ()
    
    def __init__(self, *args, **kwargs):
        # Ищем размер превью для виджета
        variations = kwargs.pop('variations', ())
        admin_variation_name = getattr(settings, 'STDIMAGE_ADMIN_VARIATION', 'admin_thumbnail')
        admin_variation = get_variation_by_name(variations, admin_variation_name)
        if admin_variation:
            self.preview_size = admin_variation.get('size')
            if callable(self.preview_size):
                self.preview_size = self.preview_size(None)
        
        # Форматируем кортеж аспектов
        aspects = kwargs.pop('aspects', ())
        if not isinstance(aspects, tuple):
            aspects = (aspects, )
        self.aspects = tuple(str(round(float(value), 4)) for value in aspects)
        
        kwargs['widget'] = StdImageWidget(
            preview_size=self.preview_size,
            aspects=self.aspects,
            crop_area=kwargs.pop('crop_area', False),
            min_dimensions=kwargs.pop('min_dimensions'),
            max_dimensions=kwargs.pop('max_dimensions'),
        )
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        final_data, delete, cropsize = data
        return (
            super().clean(final_data, initial),
            delete, 
            cropsize
        )
