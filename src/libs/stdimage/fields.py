import os
import shutil
import random
from django.conf import settings
from django.db.models import signals
from django.core.exceptions import ValidationError
from django.core.files.storage import default_storage
from django.template.defaultfilters import filesizeformat
from django.db.models.fields.files import ImageField, ImageFileDescriptor, ImageFieldFile
from django.utils.encoding import smart_text
from .forms import StdImageFormField
from .utils import *


__all__ = ['StdImageField', 'ACTION_CROP', 'ACTION_CROP_ANYWAY', 'ACTION_STRETCH_BY_WIDTH', 'ACTION_INSCRIBE']


MAX_SIZE_DEFAULT = getattr(settings,  'STDIMAGE_MAX_SIZE_DEFAULT', 12*1024*1024)
MIN_DIMENSIONS_DEFAULT = getattr(settings,  'STDIMAGE_MIN_DIMENSIONS_DEFAULT', (0, 0))
MAX_DIMENSIONS_DEFAULT = getattr(settings,  'STDIMAGE_MAX_DIMENSIONS_DEFAULT', (6000, 6000))
MAX_SOURCE_DIMENSIONS_DEFAULT = getattr(settings,  'STDIMAGE_MAX_SOURCE_DIMENSIONS_DEFAULT', (2048, 2048))


class VariationField:
    """ 
    Класс вариации у поля экземпляра модели. 
    
    Для установки атрибутов размеров картинки в шаблоне рекомендуется использовать
    свойства target_width/target_heigh, т.к. при этом нет обращения к файлу.
    """
    _width = None
    _height = None
    
    def __init__(self, name, variation, instance, storage=None):
        self.name = name
        self.variation = variation
        
        target_size = variation.get('size', (0, 0))
        if callable(target_size):
            target_size = target_size(instance)
        if not isinstance(target_size, (list, tuple)) or len(target_size) != 2:
            raise ValueError('Размер картинки некорректен: %r' % target_size)
        
        self.target_width, self.target_height = target_size
        self.storage = storage or default_storage

    @property
    def path(self):
        """Return the abs. path of the image file."""
        return self.storage.path(self.name)

    @property
    def url(self):
        """Return the url of the image file."""
        return self.storage.url(self.name)

    @property
    def url_random(self):
        """Return the url of the image file with random GET-parameter."""
        return self.storage.url(self.name) + '?_=' + str(random.random())[2:]
        
    @property
    def size(self):
        """Return the size of the image file, reported by os.stat()."""
        return self.storage.size(self.name)

    @property
    def width(self):
        if self._width is None:
            _ = self.dimensions
        return self._width

    @property
    def height(self):
        if self._width is None:
            _ = self.dimensions
        return self._height

    @property
    def dimensions(self):
        if self._width is None or self._height is None:
            path = self.path
            if os.path.exists(path):
                with open(path, 'rb') as f:
                    img = Image.open(f)
                self._width, self._height = img.size

        return self._width, self._height
        
    def clear_cache(self):
        """ Очистка закэшированных размеров картинки """
        self._width = None
        self._height = None

    def changed_size(self, instance):
        """ Возвращает True, если фактический размер картинки отличается от необходимого """
        current_size = self.dimensions
        target_size = (self.target_width, self.target_height)
        if target_size is None:
            return False
        elif callable(target_size):
            target_size = target_size(instance)
        return current_size != target_size


class StdImageFileDescriptor(ImageFileDescriptor):
    """
    The thumbnail property of the field should be accessible in instance cases
    """
    def __set__(self, instance, value):
        super().__set__(instance, value)
        self.field._set_field_variations(instance)


class StdImageFieldFile(ImageFieldFile):
    _cropsize = []
    _width = None
    _height = None

    @property
    def dimensions(self):
        if self._width is None or self._height is None:
            path = self.path
            if os.path.exists(path):
                with open(path, 'rb') as f:
                    img = Image.open(f)
                self._width, self._height = img.size

        return self._width, self._height
    
    @property
    def cropsize(self):
        return self._cropsize

    @cropsize.setter
    def cropsize(self, value):
        if isinstance(value, str):
            value = value.split(':')
        
        try:
            cropsize = [int(c) for c in value]
        except (ValueError, TypeError):
            # Некорректный формат параметров кропа
            self._cropsize = []
        else:
            if len(cropsize) == 4:
                self._cropsize = cropsize
            else:
                self._cropsize = []
                
    def recut(self, *args, crop=None):
        """ 
        Перенарезка вариаций.

        Потеряется кроп из админки, т.к. выбранная область не сохраняется.
        Можно передать параметры обрезки вручну в параметре crop, в формате
        [left, top, width, height].

        Пример:
            company.logo.recut('on_list', 'on_news')
            company.logo.recut('on_list', crop=[0, 12, 310, 177])
        """
        try:
            self.open()
            source_img = Image.open(self)
            source_img.load()
        finally:
            self.close()
            
        self.cropsize = crop
        source_format = source_img.format
    
        # Обрезаем по рамке
        temp_img = variation_crop(source_img, self.cropsize)
        
        # Обрабатываем вариации
        for variation in self.field.variations:
            if variation.get('alias_for'):
                continue
            
            if args and (not variation.get('name') in args):
                continue
            
            target_format = variation.get('format', '') or source_format
            target_format = target_format.upper()
            
            if variation.get('use_source'):
                self.field.resize_image(self.instance, variation, target_format, source_img)
            else:
                self.field.resize_image(self.instance, variation, target_format, temp_img)

    def rotate(self, angle=90, quality=SOURCE_QUALITY):
        """ 
        Поворот вариаций и исходника.

        Потеряется кроп из админки, т.к. выбранная область не сохраняется.
        Углы поворота обратны углам PIL:
            положительные - по часовой стрелке
            отрицательные - против часовой стрелке

        Пример:
            company.logo.rotate(90)
        """
        try:
            self.open()
            source_img = Image.open(self)
            source_img.load()
        finally:
            self.close()
            
        source_format = source_img.format
        info = dict(source_img.info,
                    quality=quality, )
        
        source_img = source_img.rotate(-angle, expand=1)
        
        try:
            source_img.save(self.path, source_format, optimize=1, **info)
        except IOError:
            source_img.save(self.path, source_format, **info)
        
        # Сброс закэшированных размеров
        self._width = None
        self._height = None
        
        # Обрабатываем вариации
        for variation in self.field.variations:
            if variation.get('alias_for'):
                continue
            
            target_format = variation.get('format', '') or source_format
            target_format = target_format.upper()
            
            self.field.resize_image(self.instance, variation, target_format, source_img)


class StdImageField(ImageField):
    variations = []

    attr_class = StdImageFieldFile
    descriptor_class = StdImageFileDescriptor
    default_error_messages = dict(
        ImageField.default_error_messages,
        not_enough_width='Ширина картинки не должна быть меньше {limit} пикселей',
        not_enough_height='Высота картинки не должна быть меньше {limit} пикселей',
        too_much_width='Ширина картинки не должна превышать {limit} пикселей',
        too_much_height='Высота картинки не должна превышать {limit} пикселей',
        too_big='Вес картинки не должен превышать {limit}',
    )

    def __init__(self, verbose_name=None, name=None, variations=None, *args, **kwargs):
        self.min_dimensions = kwargs.pop('min_dimensions', MIN_DIMENSIONS_DEFAULT)
        self.max_dimensions = kwargs.pop('max_dimensions', MAX_DIMENSIONS_DEFAULT)
        self.max_source_dimensions = kwargs.pop('max_source_dimensions', MAX_SOURCE_DIMENSIONS_DEFAULT)
        self.max_size = kwargs.pop('max_size', MAX_SIZE_DEFAULT)

        self.crop_area = kwargs.pop('crop_area', False)
        
        # Аспекты кропа. По умолчанию будет использоваться первый. Остальные можно использовать с помощью JS
        self.aspects = kwargs.pop('aspects', ())
        if not isinstance(self.aspects, tuple):
            self.aspects = (self.aspects, )
        self.aspects = tuple(map(float, self.aspects))

        # Форматируем вариации
        self.variations = format_variations(variations)

        super().__init__(verbose_name, name, *args, **kwargs)
        
    def formfield(self, **kwargs):
        defaults = {
            'form_class': StdImageFormField,
            'variations': self.variations,
            'crop_area': self.crop_area,
            'min_dimensions': self.min_dimensions,
            'max_dimensions': self.max_dimensions,
            'aspects': self.aspects,
        }
        defaults.update(kwargs)
        return super().formfield(**defaults)

    def save_form_data(self, instance, data):
        final_data, delete, cropsize = data
        if delete:
            final_data = ''
            self.post_delete(instance)
        
        super().save_form_data(instance, final_data)
        setattr(instance, '_{}_cropsize'.format(self.name), cropsize)

    def get_prep_value(self, value):
        if value and os.path.exists(value.path):
            return super().get_prep_value(value)
        else:
            return ''

    def contribute_to_class(self, cls, name, **kwargs):
        """Call methods for generating all operations on specified signals"""
        super().contribute_to_class(cls, name, **kwargs)
        signals.post_save.connect(self._uploaded_new_image, sender=cls)
        signals.post_init.connect(self._set_field_variations, sender=cls)
        signals.post_delete.connect(self.post_delete, sender=cls)
        
    def validate(self, value, model_instance):
        if not value:
            super().validate(value, model_instance)
            return
            
        # Для возвращения файла в исходное состояние
        image_closed = value.closed
        if value.storage.exists(value) or not image_closed:
            try:
                img = Image.open(value)
            except OSError:
                raise ValidationError('Невозможно открыть файл')
            else:
                if image_closed:
                    value.close()
                    
            img_width, img_height = img.size

            if value.cropsize:
                img_width = min(value.cropsize[2], img_width)
                img_height = min(value.cropsize[3], img_height)

            min_width, min_height = self.min_dimensions
            if min_width and img_width < min_width:
                raise ValidationError(
                    self.error_messages['not_enough_width'].format(
                        current=img_width,
                        limit=min_width
                    ),
                    code='not_enough_width',
                )
            if min_height and img_height < min_height:
                raise ValidationError(
                    self.error_messages['not_enough_height'].format(
                        current=img_height,
                        limit=min_height
                    ),
                    code='not_enough_height',
                )

            max_width, max_height = self.max_dimensions
            if max_width and img_width > max_width:
                raise ValidationError(
                    self.error_messages['too_much_width'].format(
                        current=img_width,
                        limit=max_width
                    ),
                    code='too_much_width',
                )
            if max_height and img_height > max_height:
                raise ValidationError(
                    self.error_messages['too_much_height'].format(
                        current=img_height,
                        limit=max_height
                    ),
                    code='too_much_height',
                )

            if self.max_size and value.size > self.max_size:
                raise ValidationError(
                    self.error_messages['too_big'].format(
                        current=filesizeformat(value.size),
                        limit=filesizeformat(self.max_size)
                    ),
                    code='too_big',
                )

        super().validate(value, model_instance)

    def _set_field_variations(self, instance, **kwargs):
        """
        Создает в экземпляре класса StrImageFileField поля с экземплярами вариаций
        """
        image_field = getattr(instance, self.name)
        if image_field:
            aliases = []
            filename = self.generate_filename(instance, os.path.basename(image_field.path))
            for variation in self.variations:
                if not variation.get('alias_for'):
                    variation_filename = self.build_variation_name(variation, filename)
                    variation_field = VariationField(variation_filename, variation, instance, storage=self.storage)
                    setattr(image_field, variation['name'], variation_field)
                else:
                    aliases.append(variation)
                    
            for variation in aliases:
                setattr(
                    image_field, 
                    variation['name'], 
                    getattr(image_field, variation.get('alias_for'))
                )

    @staticmethod
    def build_variation_name(variation, source_filename):
        """ Возвращает имя файла вариации """
        filepath, filename = os.path.split(source_filename)
        name, ext = os.path.splitext(filename)

        name_paths = name.split('.')
        if len(name_paths) > 1:
            name = '.'.join(name_paths[:-1])

        image_format = variation.get('format', '')
        if image_format:
            ext = '.%s' % image_format

        variation_filename = ''.join((name, '.%s' % variation['name'], ext.lower()))
        return os.path.join(filepath, variation_filename)

    @staticmethod
    def build_hash(appname, modelname, fieldname, instance_pk):
        """ Построение хэша """
        salt = '|'.join([smart_text(item) for item in (appname, modelname, fieldname, instance_pk)])
        return hashlib.md5(salt.encode('utf-8')).hexdigest()
    
    def build_source_name(self, instance, ext):
        """ Построение имени файла исходника """
        opts = instance._meta
        hash = self.build_hash(opts.app_label, opts.model_name, self.name, instance.pk)
        return '%s_%s.%s.%s' % (self.name, instance.pk, hash, ext)
    
    def build_source_path(self, instance, source_format):
        """ Построение пути к исходнику """
        if source_format == JPEG_FORMAT:
            source_format = JPEG_SAVE_FORMAT
        source_name = self.build_source_name(instance, source_format.lower())
        return self.generate_filename(instance, source_name)

    @staticmethod
    def prepare_variation(instance, image, variation, target_format):
        """ Подготовка картинки для сохранения в качестве вариации """
        image = variation_resize(instance, image, variation, target_format)
        image = variation_watermark(image, variation)
        image = variation_overlay(image, variation)
        image = variation_mask(image, variation)
        return image
    
    def resize_image(self, instance, variation, target_format, source_image):
        """ Обработка и сохранение одной вариации """
        variation_image = source_image.copy()
        
        # Цвет фона
        target_bgcolor = variation.get('background')

        # Изображение с режимом "P" нельзя сохранять в JPEG,
        # а в GIF - фон становится черным
        if variation_image.mode == 'P' and target_format in ('JPEG', 'GIF'):
            variation_image = variation_image.convert('RGBA')

        # TODO: При сохранении в GIF нужно указать индекс прозрачного цвета из палитры,
        # который хз как искать. Пока накладываем на фон
        if target_format == 'GIF':
            masked = variation_image.mode == 'RGBA'
            variation_image = put_on_bg(variation_image, variation_image.size, target_bgcolor[:3], masked)

        # Основная обработка картинок
        variation_image = self.prepare_variation(instance, variation_image, variation, target_format)
        
        # Сохранение
        image_field = getattr(instance, self.name)
        source_abspath = os.path.join(settings.MEDIA_ROOT, image_field.path)
        variation_filename = self.build_variation_name(variation, source_abspath)
        save_params = dict(
            format = target_format,
            quality = variation.get('quality', DEFAULT_QUALITY),
        )
        
        try:
            variation_image.save(variation_filename, optimize=1, **save_params)
        except IOError:
            variation_image.save(variation_filename, **save_params)

        # Очищаем закэшированные размеры картинки, т.к. они могли измениться
        variation_field = getattr(image_field, variation.get('name'))
        variation_field.clear_cache()

    def _uploaded_new_image(self, instance, **kwargs):
        """Renames the image, and calls methods to resize and create the variations."""
        image_field = getattr(instance, self.name)
        if not image_field:
            return

        filename = image_field.path
        if not os.path.exists(filename):
            return

        try:
            image_field.open()
            source_img = Image.open(image_field)
            source_format = source_img.format
            source_info = source_img.info
            source_info['quality'] = SOURCE_QUALITY
            
            new_size = limited_size(source_img.size, self.max_source_dimensions)
            if new_size is not None:
                draft = source_img.draft(None, new_size)
                if draft is None:
                    source_img = source_img.resize(new_size, Image.LINEAR)
                
            source_img.load()
        finally:
            image_field.close()
        
        # Путь к исходнику
        source_path = self.build_source_path(instance, source_format)
        source_abspath = os.path.join(settings.MEDIA_ROOT, source_path)
        source_dir = os.path.dirname(source_abspath)
        if not os.path.isdir(source_dir):
            os.makedirs(source_dir)
        
        # Флаг, что загружен новый файл
        new_file_uploaded = os.path.abspath(filename) != os.path.abspath(source_abspath)
        
        # Координаты обрезки
        image_field.cropsize = getattr(instance, '_{}_cropsize'.format(self.name), None)
        
        # Если не загрузили новый файл и не обрезали старый исходник - выходим

        # todo: тут в ФО всегда False, надо переносить новые галереи с каробки и переносить на них портал
        if not new_file_uploaded and not image_field.cropsize:
            return
        
        # Сохраняем исходник
        if new_file_uploaded:
            if new_size is None:
                # Если картинка не менялась - копируем файл
                shutil.copyfile(filename, source_abspath)
            else:
                try:
                    source_img.save(source_abspath, source_format, optimize=1, **source_info)
                except IOError:
                    source_img.save(source_abspath, source_format, **source_info)
                    
            # Удаляем загруженный исходник
            if os.path.exists(filename):
                os.remove(filename)
            
            # Записываем путь к исходнику в БД
            setattr(instance, self.attname, source_path)
            instance.save()
        
        # Обрезаем по рамке
        temp_img = variation_crop(source_img, image_field.cropsize)
        
        # Обрабатываем вариации
        for variation in self.variations:
            if variation.get('alias_for'):
                continue
            
            target_format = variation.get('format', '') or source_format
            target_format = target_format.upper()
            
            if variation.get('use_source'):
                self.resize_image(instance, variation, target_format, source_img)
            else:
                self.resize_image(instance, variation, target_format, temp_img)

    def post_delete(self, instance=None, **kwargs):
        """ Удаление всех файлов (БД не меняется) """
        image_field = getattr(instance, self.attname)
        if image_field:
            filename = image_field.path
            if os.path.exists(filename):
                os.remove(filename)
            for variation in self.variations:
                variation_filename = self.build_variation_name(variation, filename)
                if os.path.exists(variation_filename):
                    os.remove(variation_filename)
